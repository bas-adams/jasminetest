const deepFlatten = require("../app");

describe("Deep flaten array", () => {
  //let rectangle = new Rectangle(3, 4);
  let arr1 = [1, [2, 3], 4];
  let arr2 = [[1, [2, 3]], 4, [5], [6, 7]];
  let arr3 = [[1, 2, [3, 4,[5]]], 6];
  it("deep flaten step one", () => {
    expect(deepFlatten(arr1)).toEqual([1, 2, 3, 4]);
  });
  it("deep flatten step two", () => {
    expect(deepFlatten(arr2)).toEqual([1,2,3,4,5,6,7]);
  });
  it("deep flatten step three", () => {
    expect(deepFlatten(arr3)).toEqual([1,2,3,4,5,6]);
  });
});
