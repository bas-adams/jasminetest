
const deepFlatten = function(arr, result = []){
  for(let i = 0, length = arr.length; i < length; i++){
      const value = arr[i];
      if(Array.isArray(value)){
        deepFlatten(value, result);
      }else{
        result.push(value);
      }
  }
  return result;
}
module.exports = deepFlatten;
